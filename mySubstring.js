String.prototype.mySubstring = function(start, end=this.length) {
    let new_string = ''
    for (let i=start; i<end; i++) {
        new_string += this[i]
    }
    return new_string
}

console.log("hello world!".mySubstring(1, 5))
console.log("hello world!".mySubstring(6))
